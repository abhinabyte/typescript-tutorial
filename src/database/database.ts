import Mongoose from "mongoose"

let database: Mongoose.Connection

export const connect = () => {
    const uri = 'mongodb://heroku_0m7dhsj9:ktqjg1fn9qc5c1nadn877tovkq@ds023500.mlab.com:23500/heroku_0m7dhsj9'

    if (database) {
        return
    }

    Mongoose.connect(uri, {
        useNewUrlParser: true
    })

    database = Mongoose.connection

    database.once('open', async () => {
        console.log('Connected to the mongo')
    })

    database.on('error', () => {
        console.log('Not connected to the mongo')
    })
}

export const disconnect = () => {
    if (!database) {
        return
    }

    Mongoose.disconnect()
}

