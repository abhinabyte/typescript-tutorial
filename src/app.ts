import express from 'express'
import * as bodyParser from 'body-parser'

const app = express()

app.use(bodyParser.json({limit:'1mb'}))

app.get('/', (req, res) => {res.send('munu chuku')})

export {app}