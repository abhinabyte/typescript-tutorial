import {app} from './app'
import {connect} from './database/database'

connect()

app.listen(5000, () => {
    console.log("server started")
})